class CreateLogs < ActiveRecord::Migration
  def change
    create_table :logs do |t|
      t.string :name
      t.string :path
      t.string :nomenclature
      t.integer :filte_type
      t.string :host
      t.integer :port
      t.string :db
      t.string :user
      t.string :password
      t.integer :db_type

      t.timestamps null: false
    end
  end
end
